package guzmi.springboot.app.services;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;

import org.springframework.stereotype.Service;


import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.entity.Game;

import guzmi.springboot.app.exception.generic.GameKO.GameKONotFoundException;
import guzmi.springboot.app.helper.GameHelper;
import guzmi.springboot.app.repositorios.GameRepositorio;


@Service
public class GameServiceImpl implements GameService {

	//List<GameDao> listaJuegos = new ArrayList<>();
	@Autowired
	ConversionService conversion;
	
	@Autowired
	GameHelper gameHelper;
	
	@Autowired
	GameRepositorio gameRep;
	
	@Override
	public GameResponse addGame(GameRequest gameDto) {
		
		Game game =  conversion.convert(gameDto,Game.class);
		gameHelper.añadirGeneros(game, gameDto.getGenero());
		gameRep.save(game);
		return conversion.convert(game, GameResponse.class);
	}

	@Override
	public GameRequest getGame(String titulo) {
		Optional<Game> game = gameRep.findByTitulo(titulo);
		if (game.isPresent()) {
			GameRequest gameRequest = conversion.convert(game.get(), GameRequest.class);
			return gameRequest;
		}
		 throw new GameKONotFoundException();
		
	}

	@Override
	public String delteGame(String titulo) {
		Optional<Game> game = gameRep.findByTitulo(titulo);
		if (game.isPresent()) {
			gameRep.deleteById(game.get().getId());
			return "El Juego "+titulo+" ha sido borrado con exito.";
		}
		 throw new GameKONotFoundException();
	}

	@Override
	public String updateGame(GameRequest gameRequest,String titulo) {
		Optional<Game> gameOld = gameRep.findByTitulo(titulo);
		Game gameNew =  conversion.convert(gameRequest,Game.class);
		if (gameOld.isPresent()) {
			gameNew.setId(gameOld.get().getId());
			gameRep.save(gameNew);
			return "El Juego "+titulo+" ha sido actualizado con exito.";
		}
		 throw new GameKONotFoundException();
	}

}
