package guzmi.springboot.app.services.ticket;



import org.springframework.beans.factory.annotation.Autowired;

import guzmi.springboot.app.dto.request.TicketRequest;
import guzmi.springboot.app.entity.Ticket;
import guzmi.springboot.app.helper.ticket.TicketHelper;
import guzmi.springboot.app.repositorios.TicketRepositorio;

public class TicketServiceImpl implements TicketService {
	@Autowired
	TicketHelper ticketHelper;
	
	@Autowired
	TicketRepositorio ticketRep;
	

	@Override
	public String generarTicket(TicketRequest tiketRequest){
		Ticket ticket = ticketHelper.obtenerTicketEntity(tiketRequest);
		ticketRep.save(ticket);
		return "Ticket generado satisfactoriamente";
	}

}
