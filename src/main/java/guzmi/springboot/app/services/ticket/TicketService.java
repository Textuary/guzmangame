package guzmi.springboot.app.services.ticket;



import guzmi.springboot.app.dto.request.TicketRequest;

public interface TicketService {
	
	public String generarTicket(TicketRequest tiketRequest);

}
