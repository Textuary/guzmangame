package guzmi.springboot.app.services.stock;

public interface StockService {
	
	public String sumarStock(String tienda, String juego,int cantidad);
	
	public String restarStock(String tienda, String juego,int cantidad);

}
