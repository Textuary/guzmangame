package guzmi.springboot.app.services.stock;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.Stock;
import guzmi.springboot.app.entity.Tienda;
import guzmi.springboot.app.helper.stock.StockHelper;
import guzmi.springboot.app.repositorios.StockRepositorio;



@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepositorio stockRep;
	
	@Autowired
	private StockHelper stockHelper;
	
	@Override
	public String sumarStock(String nombreTienda, String tituloJuego,int cantidad) {
		Optional<Tienda> tienda = null;
		
		Optional<Game> juego = null;
		
		stockHelper.buscarTiendaYJuego(tienda, nombreTienda, juego, tituloJuego);
		
		Optional<Stock> stock = stockRep.findByIdTiendaAndIdJuego(tienda.get(), juego.get());
		Stock stockTemp;
		if (stock.isPresent()) {
			stockTemp = stock.get();
			stockTemp.setCantidad(stockTemp.getCantidad()+cantidad);
		} else {
			stockTemp = new Stock(tienda.get(),juego.get(),cantidad);
		}
		stockRep.save(stockTemp);
		return "Stock aumentado de forma correcta";
	}

	
	
	@Override
	public String restarStock(String nombreTienda, String tituloJuego,int cantidad) {
		
		Optional<Stock> stock = stockHelper.buscarStock(nombreTienda, tituloJuego);
		Stock stockTemp;
		if (stock.isPresent() && stock.get().getCantidad() >= cantidad) {
			
			stockTemp=stock.get();
			
			if(stockTemp.getCantidad()-cantidad > 0) {
				
				stockTemp.setCantidad(stockTemp.getCantidad()-cantidad);
				stockRep.save(stockTemp);
				
			}else {
				
				stockRep.delete(stock.get());
			
			}

			return "Se han eliminado"+cantidad+" "+tituloJuego+"de la tienda "+nombreTienda;
			
		} else if(stock.isPresent() && stock.get().getCantidad() <= cantidad){
			
			return "No es posible eliminar "+cantidad+" de la tienda"+nombreTienda
					+" debido a que solo dispone de "+stock.get().getCantidad()
					+" "+tituloJuego+".";
			
		}else {
			
			return "No se dispone de stock del juego "+tituloJuego+" en la tienda "+nombreTienda;
		
		}
			
	}

}
