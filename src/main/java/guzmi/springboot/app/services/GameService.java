package guzmi.springboot.app.services;


import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.response.GameResponse;

public interface GameService {
	
	public GameResponse addGame(GameRequest gameDto);
	
	public GameRequest getGame(String titulo);

	public String delteGame(String titulo);

	public String updateGame(GameRequest gameRequest,String titulo);

}
