package guzmi.springboot.app.services.tienda;

import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.request.TiendaRequest;
import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.dto.response.TiendaResponse;

public interface TiendaService {
	
	public TiendaResponse addTienda(TiendaRequest tiendaRequest);
	
	public TiendaRequest getTienda(String nombre);

	public String delteTienda(String nombre);

	public String updateTienda(TiendaRequest tiendaRequest,String nombre);

}
