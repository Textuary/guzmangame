package guzmi.springboot.app.services.tienda;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.dto.request.TiendaRequest;
import guzmi.springboot.app.dto.response.TiendaResponse;
import guzmi.springboot.app.entity.Tienda;
import guzmi.springboot.app.exception.generic.tienda.tiendaKO.TiendaKONotFoundException;
import guzmi.springboot.app.repositorios.TiendaRepositorio;

@Service
public class TiendaServiceImpl implements TiendaService {
	
	@Autowired
	ConversionService conversion;
	
	@Autowired
	TiendaRepositorio tiendaRep;

	@Override
	public TiendaResponse addTienda(TiendaRequest tiendaRequest) {
		Tienda tienda = conversion.convert(tiendaRequest, Tienda.class);
		tiendaRep.save(tienda);
		return conversion.convert(tienda, TiendaResponse.class);
	}

	@Override
	public TiendaRequest getTienda(String nombre) {
		Optional<Tienda> tienda = tiendaRep.findByNombre(nombre);
		if (tienda.isPresent()) 
			return conversion.convert(tienda.get(), TiendaRequest.class);
		 else 
			throw new TiendaKONotFoundException();
		
	}

	@Override
	public String delteTienda(String nombre) {
		Optional<Tienda> tienda = tiendaRep.findByNombre(nombre);
		if (tienda.isPresent()) 
			tiendaRep.delete(tienda.get());
		 else 
			throw new TiendaKONotFoundException();
		
		return "La tienda"+nombre+" ha sido eliminada con exito.";
	}

	@Override
	public String updateTienda(TiendaRequest tiendaRequest, String nombre) {
		Optional<Tienda> tiendaOld = tiendaRep.findByNombre(nombre);
		Tienda tiendaNew = conversion.convert(tiendaRequest, Tienda.class);
		if (tiendaOld.isPresent()) {
			tiendaNew.setId(tiendaOld.get().getId());
			tiendaRep.save(tiendaNew);
			return "La tienda "+nombre+" ha sido actualizada con exito.";
		}
		throw new TiendaKONotFoundException();
	}


}
