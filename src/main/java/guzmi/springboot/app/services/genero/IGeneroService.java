package guzmi.springboot.app.services.genero;

import java.util.Optional;

import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.entity.GeneroGames;
import guzmi.springboot.app.enumeraciones.EnumGeneros;

public interface IGeneroService {
	
	public void addGenero();
	
	public Optional<GeneroGames> getGenero(EnumGeneros genero);

}
