package guzmi.springboot.app.services.genero;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.entity.GeneroGames;
import guzmi.springboot.app.enumeraciones.EnumGeneros;
import guzmi.springboot.app.repositorios.GeneroRepositorio;

@Service
public class GeneroServiceImpl implements IGeneroService {
	
	@Autowired
	private GeneroRepositorio repositorio;

	@PostConstruct
	@Override
	public void addGenero() {
		for (EnumGeneros genero : EnumGeneros.values()) {
			repositorio.save(new GeneroGames(genero));
		}

	}
	
	@Override
	public Optional<GeneroGames> getGenero(EnumGeneros genero) {
		return repositorio.findByGenero(genero);
	}

}
