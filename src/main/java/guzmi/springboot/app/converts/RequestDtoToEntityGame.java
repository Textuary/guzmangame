package guzmi.springboot.app.converts;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.entity.Game;

public class RequestDtoToEntityGame implements Converter<GameRequest, Game>{

	@Override
	public Game convert(GameRequest source) {
		Game game = new Game();
		game.setTitulo(source.getTitulo());
		game.setDescripcion(source.getDescripcion());
		game.setLanzamiento(source.getLanzamiento());
		//game.setLikedGeneros(source.getGenero());
		return game;
	}
	
	

}
