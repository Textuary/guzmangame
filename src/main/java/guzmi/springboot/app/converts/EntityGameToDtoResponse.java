package guzmi.springboot.app.converts;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.entity.Game;

public class EntityGameToDtoResponse implements Converter<Game ,GameResponse>{


	@Override
	public GameResponse convert(Game source) {
		GameResponse gameResponse= new GameResponse();
		gameResponse.setTitulo(source.getTitulo());
		gameResponse.setLanzamiento(source.getLanzamiento());
		return gameResponse;
	}

}
