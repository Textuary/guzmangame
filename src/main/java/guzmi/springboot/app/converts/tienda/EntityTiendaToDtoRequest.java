package guzmi.springboot.app.converts.tienda;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.request.TiendaRequest;
import guzmi.springboot.app.entity.Tienda;

public class EntityTiendaToDtoRequest implements Converter<Tienda, TiendaRequest>{

	@Override
	public TiendaRequest convert(Tienda source) {
		TiendaRequest tiendaRequest = new TiendaRequest();
		tiendaRequest.setNombre(source.getNombre());
		tiendaRequest.setDireccion(source.getDireccion());
		tiendaRequest.setCodigoPostal(source.getCodigoPostal());
		return tiendaRequest;
	}



}
