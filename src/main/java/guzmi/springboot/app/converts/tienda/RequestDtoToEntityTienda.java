package guzmi.springboot.app.converts.tienda;

import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.request.TiendaRequest;
import guzmi.springboot.app.entity.Tienda;

public class RequestDtoToEntityTienda implements Converter<TiendaRequest,Tienda>{

	@Override
	public Tienda convert(TiendaRequest source) {
		Tienda tienda = new Tienda();
		tienda.setNombre(source.getNombre());
		tienda.setDireccion(source.getDireccion());
		tienda.setCodigoPostal(source.getCodigoPostal());
		tienda.setFechaRegistro(new Date());
		return tienda;
	}

}
