package guzmi.springboot.app.converts.tienda;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.response.TiendaResponse;
import guzmi.springboot.app.entity.Tienda;

public class EntityTiendaToDtoResponse implements Converter<Tienda, TiendaResponse> {

	@Override
	public TiendaResponse convert(Tienda source) {
		TiendaResponse tiendaResponse = new TiendaResponse();
		tiendaResponse.setNombre(source.getNombre());
		tiendaResponse.setFechaRegistro(source.getFechaRegistro());
		return tiendaResponse;
	}

}
