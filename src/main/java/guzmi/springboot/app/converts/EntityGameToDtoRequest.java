package guzmi.springboot.app.converts;

import org.springframework.core.convert.converter.Converter;

import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.entity.Game;

public class EntityGameToDtoRequest implements Converter<Game, GameRequest>{

	@Override
	public GameRequest convert(Game source) {
		GameRequest gameRequest= new GameRequest();
		gameRequest.setTitulo(source.getTitulo());
		gameRequest.setLanzamiento(source.getLanzamiento());
		return gameRequest;
	}

}
