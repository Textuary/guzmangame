package guzmi.springboot.app.controller;



import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.services.GameService;


@RestController
public class MyController {
	
	@Autowired
	GameService gameService;

	
	
	@GetMapping("/game")
	public ResponseEntity<Object> getGame(@RequestParam("titulo") String titulo, HttpServletRequest request){
		return ResponseEntity.status(HttpStatus.OK).body(gameService.getGame(titulo));
	}
	
	@PostMapping("/game")
	public ResponseEntity<Object> addGame(@RequestBody @Valid GameRequest gameRequest, HttpServletRequest request){
		
		GameResponse gameResponse = gameService.addGame(gameRequest);
		return ResponseEntity.status(HttpStatus.OK).body(gameResponse);
		
	}
	
	@DeleteMapping("/game")
	public ResponseEntity<Object> deleteGame(@RequestParam("titulo") String titulo, HttpServletRequest request){
		
		return ResponseEntity.status(HttpStatus.OK).body(gameService.delteGame(titulo));
		
	}
	
	@PatchMapping("/game")
	public ResponseEntity<Object> updateGame(@RequestParam("titulo") String titulo, @RequestBody @Valid GameRequest gameRequest, HttpServletRequest request){
		
		return ResponseEntity.status(HttpStatus.OK).body(gameService.updateGame(gameRequest,titulo));
		
	}
	
	

}




