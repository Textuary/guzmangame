package guzmi.springboot.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import guzmi.springboot.app.services.stock.StockService;
import guzmi.springboot.app.services.tienda.TiendaService;


@RestController
public class StockController {
	
	@Autowired
	private StockService stockService;
	
	@PostMapping("/stock")
	public String addTienda(@RequestParam("tienda") String tienda,@RequestParam("juego") String juego,@RequestParam("cantidad") int cantidad, HttpServletRequest request){
		return stockService.sumarStock(tienda, juego, cantidad);

		
	}

}
