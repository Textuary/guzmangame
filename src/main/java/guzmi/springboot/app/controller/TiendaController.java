package guzmi.springboot.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import guzmi.springboot.app.dto.request.GameRequest;
import guzmi.springboot.app.dto.request.TiendaRequest;
import guzmi.springboot.app.dto.response.GameResponse;
import guzmi.springboot.app.dto.response.TiendaResponse;
import guzmi.springboot.app.services.tienda.TiendaService;

@RestController
public class TiendaController {
	
	@Autowired
	private TiendaService tiendaService;
	
	
	@GetMapping("/tienda")
	public ResponseEntity<Object> getTienda(@RequestParam("nombre") String nombre, HttpServletRequest request){
		return ResponseEntity.status(HttpStatus.OK).body(tiendaService.getTienda(nombre));
	}
	
	@PostMapping("/tienda")
	public ResponseEntity<Object> addTienda(@RequestBody @Valid TiendaRequest tiendaRequest, HttpServletRequest request){
		
		TiendaResponse tiendaResponse = tiendaService.addTienda(tiendaRequest);
		return ResponseEntity.status(HttpStatus.OK).body(tiendaResponse);
		
	}
	
	@DeleteMapping("/tienda")
	public ResponseEntity<Object> deleteTienda(@RequestParam("nombre") String nombre, HttpServletRequest request){
		
		return ResponseEntity.status(HttpStatus.OK).body(tiendaService.delteTienda(nombre));
		
	}
	
	@PatchMapping("/tienda")
	public ResponseEntity<Object> updateTienda(@RequestParam("nombre") String nombre, @RequestBody @Valid TiendaRequest tiendaRequest, HttpServletRequest request){
		
		return ResponseEntity.status(HttpStatus.OK).body(tiendaService.updateTienda(tiendaRequest,nombre));//ResponseEntity.status(HttpStatus.OK).body(TiendaService.updateTienda(gameRequest,titulo));
		
	}

}
