package guzmi.springboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoPuertasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoPuertasApplication.class, args);
	}

}
