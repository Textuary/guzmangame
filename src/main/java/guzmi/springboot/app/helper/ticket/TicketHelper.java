package guzmi.springboot.app.helper.ticket;



import guzmi.springboot.app.dto.request.TicketRequest;

import guzmi.springboot.app.entity.Ticket;


public interface TicketHelper {
	
	public Ticket obtenerTicketEntity(TicketRequest ticket) ;

	

}
