package guzmi.springboot.app.helper.ticket;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.dto.request.TicketRequest;
import guzmi.springboot.app.entity.Cliente;
import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.Ticket;
import guzmi.springboot.app.exception.generic.GameKO.GameKONotFoundException;
import guzmi.springboot.app.repositorios.ClienteRepositorio;
import guzmi.springboot.app.repositorios.GameRepositorio;

@Service
public class TicketHelperImpl implements TicketHelper{
	
	@Autowired
	GameRepositorio gameRep;
	
	@Autowired
	ClienteRepositorio clienteRep;

	@Override
	public Ticket obtenerTicketEntity(TicketRequest ticketReq) {
		Ticket ticket = new Ticket();
		
		ticket.setNumeroTicket(generarNumeroTicket());
		ticket.setIdCliente(buscarCliente(ticketReq.getNombreCliente()));
		ticket.setLikedJuegos(buscarJuegos(ticketReq.getJuegos()));
		
		return ticket;
	}
	
	private int generarNumeroTicket() {
		Random r = new Random();
		return  r.nextInt(89999)+10000;
	}
	
	private Cliente buscarCliente(String nombre) {
		Optional<Cliente> cliente = clienteRep.findByNombre(nombre);
		if(cliente.isPresent())
			return cliente.get();
		else
			throw new GameKONotFoundException();

	}
	
	private List<Game> buscarJuegos(List<String> listaTitulosJuegos) {
		List<Game> listaJuegos = new ArrayList<>();
		Optional<Game> juego = null;
		for (String titulo : listaTitulosJuegos) {
			juego = gameRep.findByTitulo(titulo);
			if (juego.isPresent()) 
				listaJuegos.add(juego.get());
			else
				throw new GameKONotFoundException();	
		}
		return listaJuegos;
	}

}
