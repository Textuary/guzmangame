package guzmi.springboot.app.helper.stock;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.Stock;
import guzmi.springboot.app.entity.Tienda;
import guzmi.springboot.app.exception.generic.GameKO.GameKONotFoundException;
import guzmi.springboot.app.exception.generic.tienda.tiendaKO.TiendaKONotFoundException;
import guzmi.springboot.app.repositorios.GameRepositorio;
import guzmi.springboot.app.repositorios.StockRepositorio;
import guzmi.springboot.app.repositorios.TiendaRepositorio;


@Service
public class StockHelperImpl implements StockHelper {

	@Autowired
	private GameRepositorio gameRep;
	
	@Autowired
	private TiendaRepositorio tiendaRep;
	
	@Autowired
	private StockRepositorio stockRep;
	

	@Override
	public void buscarTiendaYJuego(Optional<Tienda> tienda, String nombreTienda, Optional<Game> juego,
			String tituloJuego) {
		
		tienda =tiendaRep.findByNombre(nombreTienda);
		if(!tienda.isPresent())
			throw new TiendaKONotFoundException();
				
		juego = gameRep.findByTitulo(tituloJuego);		
		if(!juego.isPresent())
			throw new GameKONotFoundException();
		
	}


	@Override
	public Optional<Stock> buscarStock(String nombreTienda, String tituloJuego) {
		Optional<Tienda> tienda =tiendaRep.findByNombre(nombreTienda);
		if(!tienda.isPresent())
			throw new TiendaKONotFoundException();
				
		Optional<Game> juego = gameRep.findByTitulo(tituloJuego);		
		if(!juego.isPresent())
			throw new GameKONotFoundException();
		
		return stockRep.findByIdTiendaAndIdJuego(tienda.get(), juego.get());
	}





}
