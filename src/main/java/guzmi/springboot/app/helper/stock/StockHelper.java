package guzmi.springboot.app.helper.stock;

import java.util.Optional;

import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.Stock;
import guzmi.springboot.app.entity.Tienda;

public interface StockHelper {
	
	public void buscarTiendaYJuego(Optional<Tienda> tienda, String nombreTienda, Optional<Game> juego,
			String tituloJuego);
	
	public Optional<Stock> buscarStock(String nombreTienda,String tituloJuego);

}
