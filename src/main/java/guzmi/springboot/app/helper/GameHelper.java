package guzmi.springboot.app.helper;

import java.util.List;

import org.springframework.http.ResponseEntity;

import guzmi.springboot.app.dto.GameDao;
import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.enumeraciones.EnumGeneros;

public interface GameHelper {
	
	public void añadirGeneros(Game game,List<EnumGeneros> listaGeneros);
}
