package guzmi.springboot.app.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import guzmi.springboot.app.dto.GameDao;
import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.GeneroGames;
import guzmi.springboot.app.enumeraciones.EnumGeneros;
import guzmi.springboot.app.exception.generic.GameKO.GameKONotFoundException;
import guzmi.springboot.app.exception.generic.GameKO.GameKONotFoundGeneroException;
import guzmi.springboot.app.services.genero.IGeneroService;

@Service
public class GameHelperImpl implements GameHelper{
	
	@Autowired
	IGeneroService generoServicio;

	@Override
	public void añadirGeneros(Game game, List<EnumGeneros> listaEnumeraciones) {
		List<GeneroGames> listaGeneros = new ArrayList<>();
		Optional<GeneroGames> objetoGenero ;
		for (EnumGeneros genero : listaEnumeraciones) {
			objetoGenero = generoServicio.getGenero(genero);
			if (objetoGenero.isPresent()) {
				listaGeneros.add(objetoGenero.get());
			}else {
				throw new GameKONotFoundGeneroException();	
			}
			
		}
		game.setLikedGeneros(listaGeneros);
		
	}


	
}
