package guzmi.springboot.app.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;



import lombok.Data;

@Entity
@Table(name = "GAMES")
@Data
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "TITULO")
	private String titulo;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "LANZAMIENTO")
	private Date lanzamiento;
	
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})	
	@JoinTable(
		name = "juegos_generos", 
		joinColumns = @JoinColumn(name = "juego_id"), 
		inverseJoinColumns = @JoinColumn(name = "genero_id"))
	List<GeneroGames> likedGeneros;
	
	@OneToMany(targetEntity=Stock.class, mappedBy="idTienda",cascade={CascadeType.PERSIST,CascadeType.MERGE}, fetch = FetchType.LAZY)
	List<Stock> stockJuego;
	
	@ManyToMany(mappedBy = "likedJuegos")
	Set<Ticket> likes;

	
	
}
