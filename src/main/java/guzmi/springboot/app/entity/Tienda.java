package guzmi.springboot.app.entity;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TIENDA")
@Data
public class Tienda {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "DIRECCION")
	private String direccion;
	
	@Column(name = "CODIGO_POSTAL")
	private int codigoPostal;
	
	@Column(name = "Fecha_Registro")
	private Date fechaRegistro;
	

	@OneToMany(targetEntity=Stock.class, mappedBy="idTienda",cascade={CascadeType.PERSIST,CascadeType.MERGE}, fetch = FetchType.LAZY)
	List<Stock> stockTienda;
	
	

}
