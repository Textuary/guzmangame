package guzmi.springboot.app.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import guzmi.springboot.app.enumeraciones.EnumGeneros;
import lombok.Data;

@Entity
@Table(name = "GENEROS")
@Data
public class GeneroGames {
	
	public GeneroGames() {
	}
	
	public GeneroGames(EnumGeneros genero) {
		this.genero = genero;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "Genero")
	@Enumerated(EnumType.STRING)
	private EnumGeneros genero;
	/*
	@Column(name = "Genero")
	private String genero;
	*/
	@ManyToMany(mappedBy = "likedGeneros")
	Set<Game> likes;
	
	

}
