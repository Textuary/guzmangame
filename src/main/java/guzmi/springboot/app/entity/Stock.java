package guzmi.springboot.app.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "STOCK")
@Data
public class Stock {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	

	@ManyToOne()
	@JoinColumn(name="id_tienda")//, insertable = false, updatable = false)
	private Tienda idTienda;
		
	
	@ManyToOne()
	@JoinColumn(name="id_juego")//, insertable = false, updatable = false)
	private Game idJuego;
	
	@Column(name = "CANTIDAD")
	private int cantidad;
	
	public Stock() {
	}

	public Stock(Tienda idTienda, Game idJuego, int cantidad) {
		this.idTienda = idTienda;
		this.idJuego = idJuego;
		this.cantidad = cantidad;
	}

	

}
