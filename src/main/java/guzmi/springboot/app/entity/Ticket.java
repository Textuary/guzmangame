package guzmi.springboot.app.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "Tickets")
@Data
public class Ticket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NUMERO_TICKET")
	private int numeroTicket;
	
	@ManyToOne()
	@JoinColumn(name="idCliente")
	private Cliente idCliente;
	
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})	
	@JoinTable(
		name = "ticket_juegos", 
		joinColumns = @JoinColumn(name = "ticket_id"), 
		inverseJoinColumns = @JoinColumn(name = "juego_id"))
	List<Game> likedJuegos;
	
//	@Column(name="JUEGO")
//	private List<String> listaJuegos;
//	
//	@Column(name="TIENDA")
//	private Tienda idTienda;
	
	
	
	
	
	
	
	
}
