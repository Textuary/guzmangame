package guzmi.springboot.app.exception.generic.GameKO;

import guzmi.springboot.app.exception.generic.GameKOException;

public class GameKONotFoundGeneroException extends GameKOException {

	private static final long serialVersionUID = 1L;
	
	private static final String MENSAJE= "Alguno de los genros introducidos no es valido.";

	public GameKONotFoundGeneroException() {
		super(MENSAJE);	
	}
}
