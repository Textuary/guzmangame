package guzmi.springboot.app.exception.generic.GameKO;

import guzmi.springboot.app.exception.generic.GameKOException;

public class GameKONotFoundException extends GameKOException{


	private static final long serialVersionUID = 1L;
	
	private static final String MENSAJE= "Juego no encontrado en nuestra base de datos";

	public GameKONotFoundException() {
		super(MENSAJE);	
	}
	
	public GameKONotFoundException(String detalle) {
		super(detalle);	
	}

}
