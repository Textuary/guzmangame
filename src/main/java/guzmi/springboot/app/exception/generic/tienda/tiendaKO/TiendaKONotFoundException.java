package guzmi.springboot.app.exception.generic.tienda.tiendaKO;

import guzmi.springboot.app.exception.generic.tienda.TiendaKOException;

public class TiendaKONotFoundException extends TiendaKOException {

	private static final long serialVersionUID = 1L;
	
	private static final String MENSAJE= "Tienda no encontrado en nuestra base de datos";

	public TiendaKONotFoundException() {
		super(MENSAJE);
	}
	
	public TiendaKONotFoundException(String detalle) {
		super(detalle);

	}


}
