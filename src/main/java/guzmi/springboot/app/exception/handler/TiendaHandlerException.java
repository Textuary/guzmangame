package guzmi.springboot.app.exception.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import guzmi.springboot.app.dto.TiendaErrorDto;
import guzmi.springboot.app.exception.generic.NoContentException;
import guzmi.springboot.app.exception.generic.NotFoundException;
import guzmi.springboot.app.exception.generic.tienda.TiendaKOException;


public class TiendaHandlerException extends ResponseEntityExceptionHandler {

	@ResponseStatus(HttpStatus.OK)
	@ExceptionHandler({TiendaKOException.class})
	@ResponseBody
	public TiendaErrorDto tiendaKO(HttpServletRequest request,TiendaKOException exception) {
		return new TiendaErrorDto("00", exception.getDetalle());
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class})
	@ResponseBody
	public TiendaErrorDto notFoundRequest(HttpServletRequest request,NotFoundException exception) {
		return new TiendaErrorDto("01", exception.getDetalle());
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({NoContentException.class})
	@ResponseBody
	public TiendaErrorDto noContentRequest(HttpServletRequest request,NoContentException exception) {
		return new TiendaErrorDto("01", exception.getDetalle());
	}
	
	
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream().map( e -> e.getDefaultMessage()).collect(Collectors.toList());
		return new ResponseEntity<>(new TiendaErrorDto("02", errorMessages.toString()),
				HttpStatus.BAD_REQUEST);
	}
}
