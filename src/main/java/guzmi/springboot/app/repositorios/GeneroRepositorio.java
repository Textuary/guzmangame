package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import guzmi.springboot.app.entity.GeneroGames;
import guzmi.springboot.app.enumeraciones.EnumGeneros;

@Repository
public interface GeneroRepositorio extends JpaRepository<GeneroGames, Long>{
	
	@Query
	Optional<GeneroGames> findByGenero(EnumGeneros genero);

}
