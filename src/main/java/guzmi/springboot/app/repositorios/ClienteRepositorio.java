package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import guzmi.springboot.app.entity.Cliente;

@Repository
public interface ClienteRepositorio extends JpaRepository<Cliente,Long>{

	@Query
	Optional<Cliente> findByNombre(String nombre);
	
}
