package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import guzmi.springboot.app.entity.Game;
import guzmi.springboot.app.entity.Stock;
import guzmi.springboot.app.entity.Tienda;

@Repository
public interface StockRepositorio extends JpaRepository<Stock, Long> {

	@Query
	Optional<Stock> findByIdTiendaAndIdJuego(Tienda tienda, Game juego);
}
