package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import guzmi.springboot.app.entity.Tienda;


@Repository
public interface TiendaRepositorio extends JpaRepository<Tienda, Long>{
	
	@Query
	Optional<Tienda> findByNombre(String nombre);

}
