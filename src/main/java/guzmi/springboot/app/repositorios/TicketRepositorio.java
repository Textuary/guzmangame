package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import guzmi.springboot.app.entity.Ticket;

@Repository
public interface TicketRepositorio extends JpaRepository<Ticket, Long>{

	@Query
	Optional<Ticket> findByNumeroTicket(int numero);
	
}
