package guzmi.springboot.app.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import guzmi.springboot.app.entity.Game;


@Repository
public interface GameRepositorio extends JpaRepository<Game, Long>{

	@Query
	Optional<Game> findByTitulo(String titulo);

}
