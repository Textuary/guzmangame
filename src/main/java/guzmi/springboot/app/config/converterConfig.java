package guzmi.springboot.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import guzmi.springboot.app.converts.EntityGameToDtoRequest;
import guzmi.springboot.app.converts.EntityGameToDtoResponse;
import guzmi.springboot.app.converts.RequestDtoToEntityGame;
import guzmi.springboot.app.converts.tienda.EntityTiendaToDtoRequest;
import guzmi.springboot.app.converts.tienda.EntityTiendaToDtoResponse;
import guzmi.springboot.app.converts.tienda.RequestDtoToEntityTienda;

@Configuration
public class ConverterConfig implements WebMvcConfigurer {

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new RequestDtoToEntityGame());
		registry.addConverter(new EntityGameToDtoResponse());
		registry.addConverter(new EntityGameToDtoRequest());
		registry.addConverter(new RequestDtoToEntityTienda());
		registry.addConverter(new EntityTiendaToDtoResponse());
		registry.addConverter(new EntityTiendaToDtoRequest());
	}

}
