package guzmi.springboot.app.dto.response;


import java.util.Date;

import lombok.Data;

@Data
public class GameResponse {
	private String titulo;
	private Date lanzamiento;
	
	public GameResponse(){
		
	}
	
}
