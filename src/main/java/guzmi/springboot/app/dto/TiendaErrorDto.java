package guzmi.springboot.app.dto;

import lombok.Data;

@Data
public class TiendaErrorDto {


	private String code;
	private String message;
	
	public TiendaErrorDto(String code, String message){
		this.code = code;
		this.message = message;
	}
	
	public TiendaErrorDto() {
		
	}
}
