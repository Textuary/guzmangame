package guzmi.springboot.app.dto.request;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import guzmi.springboot.app.enumeraciones.EnumGeneros;
import lombok.Data;

@Data
public class GameRequest {
	
	@NotBlank(message = "El titulo no puede estar vacio")
	private String titulo;
	
	private String descripcion;
	
	private Date lanzamiento;
	
	private List<EnumGeneros> genero = new ArrayList<>();
	
	//private Set<GeneroGames> genero = new HashSet<GeneroGames>();*Se salta una capa
	


}
