package guzmi.springboot.app.dto.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TiendaRequest {
	
	@NotBlank(message= "El nombre de la tienda no puede estar en blanco.")
	private String nombre;
	
	private String direccion;
	
	@Min(01000)
	@Max(52999)
	private int codigoPostal;
	

}
