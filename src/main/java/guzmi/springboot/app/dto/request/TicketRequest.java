package guzmi.springboot.app.dto.request;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TicketRequest {
	
	private int numeroTicket;
	
	@NotBlank(message = "El nombre del cliente no puede estar vacio")
	private String nombreCliente;
	
	@NotBlank(message = "El titulo del juego no puede estar vacio")
	private List<String> juegos;

}
